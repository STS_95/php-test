<?php
// Get all Pokemon
$allPokemon = json_decode(file_get_contents('https://pokeapi.co/api/v2/pokemon/?limit=500'));

// Get results
$results = $allPokemon->results;

// Get search term from URL
$searchTerm = $_REQUEST['s'];

// Search through array removing non-matches
$searchResults = array_filter($results, function($pokemon) use ($searchTerm) {
  return ( strpos($pokemon->name, $searchTerm) !== false );
});

// Empty string saved to be added to
$content = "";

// For each result get the pokemon details and create content for each
foreach ($searchResults as $key => $pokemon) {
  // Get details
  $pokemonDetails = json_decode(file_get_contents('https://pokeapi.co/api/v2/pokemon/' . $pokemon->name));

  // Create the content with link, name and sprite
  $pokemonContent =
  "<a href='pokemon.php?p=" . $pokemonDetails->name . "' class='pokemonContainer'>" .
  "<p class='pokemonName'>" . ucfirst($pokemonDetails->name) . "</p>" .
  "<img class='pokemonImg' src='" . $pokemonDetails->sprites->front_default . "'>" .
  "</a>";

  // Add each pokemon to the overall content
  $content = $content . $pokemonContent;
}

// Create prev and next buttons when applicable
$nextPrev = "<div id='pokemonNav'>";


if(isset($allPokemon->previous)){
  $url = "'" . $allPokemon->previous . "'";
  $nextPrev = $nextPrev . "<button id='previousButton' onclick='prevNext(this.id)' link=" . $url .">Previous</button>";
}
if(isset($allPokemon->next)){
  $url = "'" . $allPokemon->next . "'";
  $nextPrev = $nextPrev . "<button id='nextButton' onclick='prevNext(this.id)' link=" . $url .">Next</button>";
}

$nextPrev = $nextPrev . "</div>";

// return the results
echo $content . $nextPrev;
?>
