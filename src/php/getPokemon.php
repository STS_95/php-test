<?php

    $pokemonDetails = json_decode(file_get_contents('https://pokeapi.co/api/v2/pokemon/' .  $_REQUEST["p"]));

    $pokemonContent =
      "<a href='pokemon.php?v=" . $pokemonDetails->name . "' class='pokemonContainer'>" .
      "<p class='pokemonName'>" . ucfirst($pokemonDetails->name) . "</p>" .
      "<img class='pokemonImg' src='" . $pokemonDetails->sprites->front_default . "'>" .
      "</a>";

    echo $pokemonContent;
?>
