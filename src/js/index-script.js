function getAllPokemon(url) {
  // Create new request
  var xmlGetAllPokemon = new XMLHttpRequest();

  // On ready state
  xmlGetAllPokemon.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      // Display pokemon
      document.getElementById("pokemonList").innerHTML  = this.response;
    }
  };

  // Open and send request
  xmlGetAllPokemon.open("GET", url, true);
  xmlGetAllPokemon.send();
}

// Once document has loaded get first set of pokemon
$(document).ready(function () {
  getAllPokemon("../src/php/getAllPokemon.php?l=https://pokeapi.co/api/v2/pokemon/");
});

// When previous or next buttons are pressed grab the URL and send to getAllPokemon function
function prevNext(id){
      var link = document.getElementById(id).getAttribute("link");

      getAllPokemon("../src/php/getAllPokemon.php?l=" + link);
};
