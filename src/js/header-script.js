$('#searchInput').on('input', function(){
  // Get search input
  var searchTerm = document.getElementById('searchInput').value;

  // Check if search is blank or not
  if(searchTerm != ""){

  // Create new request
  var xmlGetSearchResults = new XMLHttpRequest();

  // On ready state
  xmlGetSearchResults.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      // Display results
      console.log(this.response);
      document.getElementById("pokemonList").innerHTML  = this.response;
    }
  };

  // Open and send request
  xmlGetSearchResults.open("GET", '../src/php/getSearchResults.php?s=' + searchTerm, true);
  xmlGetSearchResults.send();
}
});
