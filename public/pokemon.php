<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Pokédex</title>

  <link rel="stylesheet" type="text/css" href="../src/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../src/css/pokemon-view-styles.css">
</head>
<body>
  <!-- Nav Bar -->
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="index.php">
      <img src="../src/img/logo.png" alt="Pokemon Logo" id="navLogo">
    </a>
  </nav>


  <!-- Display Pokemon Details -->
  <article id="pokemonDetails">
    <!-- Get Pokemon from URL and do an API call to get the details json and decode it -->
    <?php
    $pokemonDetails = json_decode(file_get_contents('https://pokeapi.co/api/v2/pokemon/' .  $_REQUEST["p"]));
    ?>

    <div class="row">
      <div class="col-md-5" id="spritesContainer">
        <!-- Display all available sprites -->
        <?php
        foreach ($pokemonDetails->sprites as $key => $sprite) {
          echo "<img class='sprite' src='" . $sprite . "'>";
        }
        ?>
      </div>
      <div class="col-md-7">
        <?php
        // Display Pokemon's name
        echo "<h1 id='pokemonName'>" . ucfirst($pokemonDetails->name)  . "</h1>";
        // Display Pokemon's species name
        echo "<p><b>Species: </b>" . ucfirst($pokemonDetails->species->name) ."</p>";
        // Display Pokemon's height
        echo "<p><b>Height: </b>" . $pokemonDetails->height ."</p>";
        // Display Pokemon's weight
        echo "<p><b>Weight: </b>" . $pokemonDetails->weight ."</p>";

        // Display all of the Pokemon's types
        echo "<p><b>Types: </b>";
        foreach ($pokemonDetails->types as $key => $type) {
          if($key == 0){
            echo $type->type->name;
          }else{
            echo ', ' . $type->type->name;
          }
        }
        echo "</p>";

        // Display all of the Pokemon's abilities
        echo "<p><b>Abilities: </b>";
        foreach ($pokemonDetails->abilities as $key => $ability) {
          if($key == 0){
            echo $ability->ability->name;
          }else{
            echo ', ' . $ability->ability->name;
          }
        }
        echo "</p>";


        // Display the spirite of each of this pokemons evolutions with a link to their details
        echo "<p><b>Evolution:</b></p>";
        $species = json_decode(file_get_contents($pokemonDetails->species->url));
        $evolution = json_decode(file_get_contents($species->evolution_chain->url));

        if(isset($evolution->chain->species->name)){
          $evolution1 = json_decode(file_get_contents('https://pokeapi.co/api/v2/pokemon/' . $evolution->chain->species->name));
          $evolution1Content =
          "<a href='pokemon.php?p=" . $evolution1->name . "' class='pokemonContainer'>" .
          "<img class='pokemonImg' src='" . $evolution1->sprites->front_default . "'>" .
          "</a>";

          echo $evolution1Content;
        }

        if(isset($evolution->chain->evolves_to[0]->species->name)) {
          $evolution2 = json_decode(file_get_contents('https://pokeapi.co/api/v2/pokemon/' . $evolution->chain->evolves_to[0]->species->name));
          $evolution2Content =
          "<a href='pokemon.php?p=" . $evolution2->name . "' class='pokemonContainer'>" .
          "<img class='pokemonImg' src='" . $evolution2->sprites->front_default . "'>" .
          "</a>";

          echo $evolution2Content;
        }

        if (isset($evolution->chain->evolves_to[0]->evolves_to[0]->species->name)) {

          $evolution3 = json_decode(file_get_contents('https://pokeapi.co/api/v2/pokemon/' . $evolution->chain->evolves_to[0]->evolves_to[0]->species->name));
          $evolution3Content =
          "<a href='pokemon.php?p=" . $evolution3->name . "' class='pokemonContainer'>" .
          "<img class='pokemonImg' src='" . $evolution3->sprites->front_default . "'>" .
          "</a>";

          echo $evolution3Content;
        }

        ?>
      </div>
    </div>
  </article>
</body>
</html>
<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script src="../src/bootstrap/js/bootstrap.min.js"></script>
<script src="../src/js/pokemon-view-script.js"></script>
<script src="../src/js/header-script.js"></script>
