<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Pokédex</title>

  <link rel="stylesheet" type="text/css" href="../src/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../src/css/index-styles.css">
</head>
<body>
  <!-- Nav Bar -->
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="index.php">
      <img src="../src/img/logo.png" alt="Pokemon Logo" id="navLogo">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <div class="row" id="searchContainer">
        <input class="form-control col-md-12" id="searchInput" type="search" placeholder="Search.. i.e Charmander, Pikachu etc." aria-label="Search">
      </div>
    </div>
  </nav>


  <!-- Main Section for displaying Pokemon -->
  <section id="pokemonListing">
    <div id="pokemonList">

    </div>
  </section>
</body>
</html>
<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script src="../src/bootstrap/js/bootstrap.min.js"></script>
<script src="../src/js/index-script.js"></script>
<script src="../src/js/header-script.js"></script>
